<?php
return [
    'cache' => [
        'scheme'=>'tcp',
        'host'=>'app_redis',
        'port'=>6379,
        'password'=>null,
        'database'=>1
    ]
];
