<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 21/11/17
 * Time: 00:03.
 */

namespace App\Cache\Exception;

class InvalidArgumentException extends \Exception implements \Psr\SimpleCache\InvalidArgumentException
{
}
