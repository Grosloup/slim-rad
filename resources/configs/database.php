<?php

return [
    'db_params'     => [
        'development' => [
            'driver'  => 'pdo_sqlite',
            'user'    => '',
            'password'=> '',
            'path'    => __DIR__ . '/../var/datas/database.sqlite',
            'memory'  => false
        ],
        'production' => [
            'driver'  => 'pdo_mysql',
            'dbname'  => getenv('MYSQL_DBNAME'),
            'dbhost'  => getenv('MYSQL_HOST'),
            'user'    => getenv('MYSQL_USER'),
            'password'=> getenv('MYSQL_PASSWORD'),
            'port'    => getenv('MYSQL_PORT'),
            'charset' => getenv('MYSQL_CHARSET')
        ]
    ],
    'configs_files' => [
        __DIR__ . '/database_configs'
    ],
];
