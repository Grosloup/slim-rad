<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 11:31.
 */

namespace Test\App\Auth;

use App\Auth\PlainTextAuth;
use App\Session\ArraySessionStorage;
use PHPUnit\Framework\TestCase;

class PlainTextAuthTest extends TestCase
{
    private $session;

    private $params = [];

    public function setUp()
    {
        $this->session = new ArraySessionStorage();
        $this->params = [
            'users'       => [
                ['username' => 'admin', 'password' => '$2y$13$ywpcIqJDFYPyX7.4XVlT/OzdZ/TPSgTQwZxNu3X5Vymt3ZdjFkZki'],
                ['username' => 'admin2', 'password' => '$2y$13$ywpcIqJDFYPyX7.4XVlT/OzdZ/TPSgTQwZxNu3X5Vymt3ZdjFkZki'],
            ],
            'session.key' => 'admin',
        ];
    }

    /**
     * @test
     */
    public function testUserExists()
    {
        $auth = new PlainTextAuth($this->params, $this->session);
        $this->assertNull($auth->userExists('noexists'));
        $this->assertNotNull($auth->userExists('admin'));
        $this->assertArrayHasKey('username', $auth->userExists('admin'));
        $this->assertNotNull($auth->userExists('admin2'));
        $this->assertArrayHasKey('username', $auth->userExists('admin2'));
    }

    /**
     * @test
     */
    public function testCheckUserWhenNotRegistred()
    {
        $auth = new PlainTextAuth($this->params, $this->session);
        $this->assertFalse($auth->checkUser());
    }

    /**
     * @test
     */
    public function testCheckUserWhenRegistred()
    {
        $auth = new PlainTextAuth($this->params, $this->session);
        $auth->getUser('admin', 'password');
        $this->assertTrue($auth->checkUser());
    }

    /**
     * @test
     */
    public function testGetUser()
    {
        $auth = new PlainTextAuth($this->params, $this->session);
        $user = $auth->getUser('admin', 'password');
        $this->assertTrue($user);
        $this->assertSame('admin', $auth->getCurrentUser()['username']);
    }

    /**
     * @test
     */
    public function testUnsetUser()
    {
        $auth = new PlainTextAuth($this->params, $this->session);
        $auth->getUser('admin', 'password');
        $this->assertSame('admin', $auth->getCurrentUser()['username']);
        $auth->unsetUser();
        $this->assertNull($auth->getCurrentUser());
    }
}
