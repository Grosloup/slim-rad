<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/11/17
 * Time: 19:10.
 */

namespace App\Event;

use App\Event\Psr14\EventInterface;

/**
 * Class Event.
 */
class Event implements EventInterface
{
    /**
     * @var string
     */
    protected $name = '';
    /**
     * @var null
     */
    protected $target;
    /**
     * @var array
     */
    protected $params = [];
    /**
     * @var bool
     */
    protected $isPropagationStopped = false;

    /**
     * Event constructor.
     *
     * @param $name
     * @param null  $target
     * @param array $params
     * @param bool  $isPropagationStopped
     */
    public function __construct($name, $target = null, $params = [], $isPropagationStopped = false)
    {
        $this->name = $name;
        $this->target = $target;
        $this->params = $params;
        $this->isPropagationStopped = $isPropagationStopped;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     *
     * @return Event
     */
    public function setParams(array $params = [])
    {
        $this->params = $params;

        return $this;
    }

    public function getParam($name)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : null;
    }

    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }

    /**
     * @param bool $flag
     */
    public function stopPropagation($flag)
    {
        $this->isPropagationStopped = $flag;
    }

    /**
     * @return bool
     */
    public function isPropagationStopped()
    {
        return $this->isPropagationStopped;
    }
}
