<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 11:09.
 */

namespace App\Session;

/**
 * Class NativeSessionStorage.
 */
class NativeSessionStorage implements SessionStorageInterface
{
    /**
     * @var array
     */
    private $datas = [];

    /**
     * NativeSessionStorage constructor.
     */
    public function __construct()
    {
        $this->datas = &$_SESSION;
    }

    /**
     * @param string $key
     * @param null   $default
     *
     * @return mixed|null
     */
    public function getKey($key = '', $default = null)
    {
        if (array_key_exists($key, $this->datas)) {
            return $this->datas[$key];
        }

        return $default;
    }

    /**
     * @param string $key
     * @param null   $value
     */
    public function setKey($key = '', $value = null)
    {
        $this->datas[$key] = $value;
    }

    /**
     * @param string $key
     */
    public function unset($key = '')
    {
        if (array_key_exists($key, $this->datas)) {
            unset($this->datas[$key]);
        }
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has($key = '')
    {
        return array_key_exists($key, $this->datas);
    }
}
