<?php

namespace App\Controller;

use App\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/11/17
 * Time: 19:24.
 */
class IndexController extends BaseController
{
    /**
     * @param Request    $request
     * @param Response   $response
     * @param mixed|null $args
     *
     * @return Response $response
     */
    public function __invoke(Request $request, Response $response, $args = null)
    {
        return $this->render($response, 'index.html.twig');
    }
}
