<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 14:33.
 */

namespace App\Handler;

use Slim\Handlers\AbstractHandler;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use UnexpectedValueException;

/**
 * Class NotFoundHandler.
 */
class NotFoundHandler extends AbstractHandler
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * NotFoundHandler constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response)
    {
        $contentType = $this->determineContentType($request);

        switch ($contentType) {
            case 'text/html':
                $response = $this->renderHtmlNotFoundOutput($response);
                break;
            case 'application/json':
                $response = $this->renderJsonNotFoundOutput($response);
                break;
            case 'text/xml':
            case 'application/xml':
                $response = $this->renderXmlNotFoundOutput($response);
                break;
            default:
                throw new UnexpectedValueException('Cannot render unknown content type ' . $contentType);
        }

        return $response->withStatus(404)
                        ->withHeader('Content-Type', $contentType);
    }

    /**
     * @param $response
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function renderHtmlNotFoundOutput($response)
    {
        return $this->twig->render($response, 'errors/404.html.twig');
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    protected function renderJsonNotFoundOutput(Response $response)
    {
        return $response->withJson(['error' => 'Not found']);
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    protected function renderXmlNotFoundOutput(Response $response)
    {
        return $response->write('<root><message>Not found</message></root>');
    }
}
