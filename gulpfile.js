var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')
var uglifycss = require('gulp-uglifycss')
var rename = require('gulp-rename')
var notify = require('gulp-notify')

var scssSrc = './scss/*.scss'
var cssDest = './public/css'
var autoprefixerBrowsersOptions = {browsers: ['last 3 versions']}
var jsSrc = './js_src/*.js'
var jsDest = './public/js'
var jsDestName = 'app'

gulp.task('styles', function () {
  gulp
    .src(scssSrc)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerBrowsersOptions))
    .pipe(gulp.dest(cssDest))
    .pipe(uglifycss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(cssDest))
    .pipe(notify({message: 'TASK: "styles" Completed!', onLast: true}))
})

gulp.task('js', function () {
  gulp
    .src(jsSrc)
    .pipe(concat(jsDestName + '.js'))
    .pipe(uglify())
    .pipe(gulp.dest(jsDest))
})

gulp.task('default', ['styles', 'js'], function () {
  gulp.watch(scssSrc, ['styles'])
  gulp.watch(jsSrc, ['js'])
})
