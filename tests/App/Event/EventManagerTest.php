<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/11/17
 * Time: 20:08.
 */

namespace Test\App\Event;

use App\Event\EventManager;
use App\Event\Psr14\EventInterface;
use App\Event\Psr14\EventManagerInterface;
use PHPUnit\Framework\TestCase;

class EventManagerTest extends TestCase
{
    /**
     * @var EventManagerInterface
     */
    private $eventManager;

    public function setUp()
    {
        $this->eventManager = new EventManager();
    }

    /**
     * @test
     */
    public function canAttachListenerAndTrigger()
    {
        $this->eventManager->attach('event', function () { echo 'attached!'; });
        $this->eventManager->trigger('event');
        $this->expectOutputString('attached!');
    }

    /**
     * @test
     */
    public function canDetachListener()
    {
        $listener = function () { echo 'detached!'; };
        $this->eventManager->attach('event', $listener);
        $this->eventManager->detach('event', $listener);
        $this->eventManager->trigger('event');
        $this->expectOutputString('');
    }

    /**
     * @test
     */
    public function canClearListeners()
    {
        $this->eventManager->attach('event1', function () { echo '1attached!'; });
        $this->eventManager->attach('event1', function () { echo '1attached!'; });
        $this->eventManager->attach('event2', function () { echo '2attached!'; });
        $this->eventManager->clearListeners('event1');
        $this->eventManager->trigger('event1');
        $this->eventManager->trigger('event2');
        $this->expectOutputString('2attached!');
    }

    /**
     * @test
     */
    public function canTriggerByPriority()
    {
        $this->eventManager->attach('event1', function () { echo 'troisieme'; }, -1);
        $this->eventManager->attach('event1', function () { echo 'premier'; }, 100);
        $this->eventManager->attach('event1', function () { echo 'deuxieme'; }, 2);
        $this->eventManager->trigger('event1');
        $this->expectOutputString('premierdeuxiemetroisieme');
    }

    /**
     * @test
     */
    public function canStopPropagation()
    {
        $this->eventManager->attach('event1', function () { echo 'troisieme'; }, -1);
        $this->eventManager->attach('event1', function () { echo 'premier'; }, 100);
        $this->eventManager->attach('event1', function (EventInterface $event) { echo 'deuxieme'; $event->stopPropagation(true); }, 2);
        $this->eventManager->trigger('event1');
        $this->expectOutputString('premierdeuxieme');
    }
}
