#! /bin/bash

PHPAPI=$(phpize -v | grep Zend\ M | grep -oE '([0-9]+)')

pecl install xdebug

echo "[xdebug]" >> /etc/php/7.1/cli/php.ini
echo "zend_extension = /usr/lib/php/$PHPAPI/xdebug.so" >> /etc/php/7.1/cli/php.ini
echo "xdebug.var_display_max_children = -1" >> /etc/php/7.1/cli/php.ini
echo "xdebug.var_display_max_data = -1" >> /etc/php/7.1/cli/php.ini
echo "xdebug.var_display_max_depth = 256" >> /etc/php/7.1/cli/php.ini

echo "[xdebug]" >> /etc/php/7.1/fpm/php.ini
echo "zend_extension = /usr/lib/php/$PHPAPI/xdebug.so" >> /etc/php/7.1/fpm/php.ini
echo "xdebug.var_display_max_children = -1" >> /etc/php/7.1/fpm/php.ini
echo "xdebug.var_display_max_data = -1" >> /etc/php/7.1/fpm/php.ini
echo "xdebug.var_display_max_depth = 256" >> /etc/php/7.1/fpm/php.ini
