<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 11:07.
 */

namespace App\Session;

interface SessionStorageInterface
{
    public function getKey($key = '');

    public function setKey($key = '', $value = null);

    public function unset($key = '');

    public function has($key = '');
}
