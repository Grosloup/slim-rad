<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 16:57.
 */

namespace Test\App\Middleware;

use App\Middleware\CsrfMiddleware;
use App\Session\ArraySessionStorage;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Slim\Http\Body;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Uri;

class CsrfMiddlewareTest extends TestCase
{
    /**
     * @test
     */
    public function testPassGet()
    {
        $csrfMiddleware = new CsrfMiddleware($this->makeContainer(), $this->makeSession());

        /** @var Response $response */
        $response = $csrfMiddleware($this->makeRequest('GET'), $this->makeResponse(200), $this->next());

        $this->assertSame(200, $response->getStatusCode());
    }

    private function makeContainer()
    {
        return $this->getMockBuilder(ContainerInterface::class)->getMock();
    }

    private function makeSession()
    {
        return new ArraySessionStorage();
    }

    private function makeRequest($method = 'GET', $params = [])
    {
        $request = new Request(
            $method,
            Uri::createFromString('/'),
            new Headers(),
            [],
            Environment::mock()->all(),
            $body = new Body(fopen('php://temp', 'r+')),
            []
        );

        return $request->withParsedBody($params);
    }

    private function makeResponse($status = 200)
    {
        return (new Response())->withStatus($status);
    }

    private function next()
    {
        return function ($request, $response) {
            return $response;
        };
    }

    /**
     * @test
     */
    public function testPostDoesntPass()
    {
        $csrfMiddleware = new CsrfMiddleware($this->makeContainer(), $this->makeSession());

        /** @var Response $response */
        $response = $csrfMiddleware($this->makeRequest('POST', []), $this->makeResponse(200), $this->next());

        $this->assertSame(400, $response->getStatusCode());
        $this->assertSame('csrf injection detected !', (string) $response->getBody());
    }

    /**
     * @test
     */
    public function testValidToken()
    {
        $csrfMiddleware = new CsrfMiddleware($this->makeContainer(), $this->makeSession());
        $token = $csrfMiddleware->generateToken();
        $params = [
            $csrfMiddleware->getFormKey() => $token,
        ];
        /** @var Response $response */
        $response = $csrfMiddleware($this->makeRequest('POST', $params), $this->makeResponse(200), $this->next());
        $this->assertSame(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function testInvalidToken()
    {
        $csrfMiddleware = new CsrfMiddleware($this->makeContainer(), $this->makeSession());
        $token = $csrfMiddleware->generateToken();
        $params = [
            $csrfMiddleware->getFormKey() => 'aazerty',
        ];
        /** @var Response $response */
        $response = $csrfMiddleware($this->makeRequest('POST', $params), $this->makeResponse(200), $this->next());
        $this->assertSame(400, $response->getStatusCode());
        $this->assertSame('csrf injection detected !', (string) $response->getBody());
    }

    /**
     * @test
     */
    public function testLimit()
    {
        $session = $this->makeSession();
        $csrfMiddleware = new CsrfMiddleware($this->makeContainer(), $session);
        $tokens = [];
        for ($i = 0; $i < 100; ++$i) {
            $tokens[] = $csrfMiddleware->generateToken();
        }

        $params = [
            $csrfMiddleware->getFormKey() => $tokens[99],
        ];
        $response = $csrfMiddleware($this->makeRequest('POST', $params), $this->makeResponse(200), $this->next());
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(50, count($session->getKey($csrfMiddleware->getSessionKey())));
        $this->assertSame($tokens[99], $session->getKey($csrfMiddleware->getSessionKey())[48]);
    }
}
