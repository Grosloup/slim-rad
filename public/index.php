<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/11/17
 * Time: 19:05
 */
session_start();
require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../resources/bootstrap.php";
$app->run();
