<?php

namespace App;

use Psr\Container\ContainerInterface;

/**
 * Class BaseController.
 */
class BaseController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * BaseController constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Render Twig template.
     *
     * @param $response
     * @param string $template
     * @param array  $datas
     *
     * @return mixed
     */
    protected function render($response, $template = '', $datas = [])
    {
        return $this->container->view->render($response, $template, $datas);
    }

    /**
     * @param $routeName
     * @param array $data
     * @param array $queryParams
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     *
     * @return mixed
     */
    protected function pathFor($routeName, array $data = [], array $queryParams = [])
    {
        if (!$this->container->has('router')) {
            throw new \RuntimeException('router is not defined on container');
        }

        return $this->container->get('router')->pathFor($routeName, $data, $queryParams);
    }

    /**
     * @param string $level
     * @param string $message
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function addMessage($level = 'danger', $message = '')
    {
        if (!$this->container->has('flash')) {
            throw new \RuntimeException('flash is not defined on container');
        }
        $this->container->get('flash')->addMessage($level, $message);
    }
}
