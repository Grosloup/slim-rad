<?php

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$app = new \Slim\App(
    [
        'settings' => [
            'displayErrorDetails' => 'production' === getenv('ENV_MODE') ? false : true,
        ],
    ]
);

require_once __DIR__ . '/container.php';
require_once __DIR__ . '/middlewares.php';
require_once __DIR__ . '/routes/web.php';
