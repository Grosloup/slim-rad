<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/11/17
 * Time: 19:29.
 */

namespace App\Event;

use App\Event\Psr14\EventInterface;
use App\Event\Psr14\EventManagerInterface;

/**
 * Class EventManager.
 */
class EventManager implements EventManagerInterface
{
    /**
     * @var array
     */
    protected $listeners = [];

    /**
     * Attaches a listener to an event
     * Highest priority => first to be triggered.
     *
     * @param string   $event    the event to attach too
     * @param callable $callback a callable function
     * @param int      $priority the priority at which the $callback executed
     *
     * @return bool true on success false on failure
     */
    public function attach($event, $callback, $priority = 0)
    {
        $this->listeners[$event][] = [
            'priority' => $priority,
            'callback' => $callback,
        ];

        return true;
    }

    /**
     * Detaches a listener from an event.
     *
     * @param string   $event    the event to attach too
     * @param callable $callback a callable function
     *
     * @return bool true on success false on failure
     */
    public function detach($event, $callback)
    {
        $this->listeners[$event] = array_filter(
            $this->listeners[$event],
            function ($listener) use ($callback) {
                return $listener['callback'] !== $callback;
            }
        );

        return true;
    }

    /**
     * Clear all listeners for a given event.
     *
     * @param string $event
     */
    public function clearListeners($event)
    {
        $this->listeners[$event] = [];
    }

    /**
     * Trigger an event.
     *
     * Can accept an EventInterface or will create one if not passed
     *
     * @param string|EventInterface $event
     * @param object|string         $target
     * @param array|object          $argv
     *
     * @return mixed
     */
    public function trigger($event, $target = null, $argv = [])
    {
        if (is_string($event)) {
            $event = new Event($event, $target, $argv);
        }
        if (isset($this->listeners[$event->getName()])) {
            $listeners = $this->listeners[$event->getName()];
            //Highest priority => first to be triggered
            usort($listeners, function ($a, $b) { return $b['priority'] - $a['priority']; });

            foreach ($listeners as $listener) {
                if ($event->isPropagationStopped()) {
                    break;
                }
                call_user_func($listener['callback'], $event);
            }
        }
    }
}
