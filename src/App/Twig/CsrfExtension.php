<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 19:27.
 */

namespace App\Twig;

use App\Middleware\CsrfMiddleware;

class CsrfExtension extends \Twig_Extension
{
    /**
     * @var CsrfMiddleware
     */
    private $csrfMiddleware;

    /**
     * CsrfExtension constructor.
     *
     * @param CsrfMiddleware $csrfMiddleware
     */
    public function __construct(CsrfMiddleware $csrfMiddleware)
    {
        $this->csrfMiddleware = $csrfMiddleware;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('csrf_field', [$this, 'csrfField'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @throws \Exception
     *
     * @return string
     */
    public function csrfField()
    {
        return "<input type='hidden' name='" . $this->csrfMiddleware->getFormKey(
            ) . "' value='" . $this->csrfMiddleware->generateToken() . "' />";
    }
}
