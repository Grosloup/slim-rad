<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 09:56.
 */

namespace App\Auth;

use App\Session\SessionStorageInterface;

/**
 * Class PlainTextAuth.
 */
class PlainTextAuth implements AuthInterface
{
    /**
     * @var array
     */
    private $params = [];
    /**
     * @var SessionStorageInterface
     */
    private $sessionStorage;

    /**
     * PlainTextAuth constructor.
     *
     * @param array                   $params
     * @param SessionStorageInterface $sessionStorage
     */
    public function __construct($params, SessionStorageInterface $sessionStorage)
    {
        $this->params = $params;

        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @return bool
     */
    public function checkUser()
    {
        $currentUser = $this->getCurrentUser();

        if (empty($currentUser) || empty($currentUser['username']) || empty($currentUser['password'])) {
            return false;
        }

        if ($user = $this->userExists($currentUser['username'])) {
            return $currentUser['password'] === $user['password'];
        }
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return $this->sessionStorage->getKey($this->params['session.key']);
    }

    /**
     * @param string $username
     *
     * @return null|array
     */
    public function userExists($username = '')
    {
        $users = $this->params['users'];
        foreach ($users as $user) {
            if ($user['username'] === $username) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    public function getUser($username = '', $password = '')
    {
        if ($user = $this->userExists($username)) {
            if (password_verify($password, $user['password'])) {
                $this->sessionStorage
                    ->setKey(
                        $this->params['session.key'],
                        $user
                    );

                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getSessionKey()
    {
        return $this->params['session.key'];
    }

    public function unsetUser()
    {
        $this->sessionStorage->unset($this->params['session.key']);
    }
}
