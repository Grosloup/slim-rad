<?php
/*
 * container
 */

use App\Auth\PlainTextAuth;
use App\Handler\NotFoundHandler;
use App\Middleware\CsrfMiddleware;
use App\Session\NativeSessionStorage;
use App\Twig\CsrfExtension;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Predis\Client;
use Slim\Flash\Messages;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

$container = $app->getContainer();

$bases = require_once __DIR__ . '/configs/bases.php';

$container['configs'] = [
    'env'      => empty($bases['env']) ? 'development' : $bases['env'], // production
    'twig'     => require_once __DIR__ . '/configs/twig.php',
    'doctrine' => require_once __DIR__ . '/configs/database.php',
    'redis' => require_once __DIR__ . '/configs/redis.php',
];

/*
 * terminal hash password
 * php -r "echo password_hash('password', PASSWORD_BCRYPT, ['cost' => 13]) . PHP_EOL;"
 */
$container['security'] = require_once __DIR__ . '/configs/security.php';

$container['notFoundHandler'] = function ($container) {
    return new NotFoundHandler($container->view);
};

$container['session'] = function ($container) {
    return new NativeSessionStorage();
};


$container['redis.cache'] = function ($container) {
    throw new \Exception("NOT TESTED !");
  $redis = new Client([
      'scheme'=>$container['configs']['redis']['cache']['scheme'],
      'host'=>$container['configs']['redis']['cache']['host'],
      'port'=>$container['configs']['redis']['cache']['port'],
      'password'=>$container['configs']['redis']['cache']['password'],
      'database'=>$container['configs']['redis']['cache']['database'],
  ]);

  return $redis;
};

$container['secure.admin.auth'] = function ($container) {
    $auth = new PlainTextAuth($container['security']['admin'], $container['session']);

    return $auth;
};

$container['flash'] = function ($container) {
    return new Messages();
};

$container['csrf'] = function ($container) {
    return new CsrfMiddleware($container, $container['session']);
};

$container['view'] = function ($container) {
    $view = new Twig(
        $container['configs']['twig']['templates'],
        [
            'cache' => $container['configs']['env'] === 'production' ? $container['configs']['twig']['cache_dir'] : false,
            'debug' => $container['configs']['env'] === 'production' ? false : true,
        ]
    );

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new TwigExtension($container['router'], $basePath));
    $view->addExtension(new CsrfExtension($container['csrf']));

    $view->getEnvironment()->addGlobal('flash', $container->flash);
    $view->getEnvironment()->addGlobal('session', $_SESSION);
    if ($container['configs']['env'] === 'development') {
        $view->addExtension(new \Twig_Extension_Debug());
    }

    return $view;
};

$container['doctrine'] = function ($container) {
    $config = Setup::createXMLMetadataConfiguration(
        $container['configs']['doctrine']['configs_files'],
        $container['configs']['env'] === 'production' ? false : true
    );
    $dbParams = $container['configs']['doctrine']['db_params'][$container['configs']['env']];
    $entityManager = EntityManager::create(
        $dbParams,
        $config
    );

    return $entityManager;
};

$app->add($container['csrf']);
