<?php

return [
    'admin' => [
        'users'       => [
            ['username' => 'admin', 'password' => '$2y$13$ywpcIqJDFYPyX7.4XVlT/OzdZ/TPSgTQwZxNu3X5Vymt3ZdjFkZki'],
        ],
        'session.key' => 'admin',
        'paths'       => [
            'signin_uri'           => '/back-office/signin',
            'signin_check_uri'     => '/back-office/signin_check',
            'signout_uri'          => '/back-office/signout',
            'success_redirect_uri' => '/back-office',
        ],
    ],
];
