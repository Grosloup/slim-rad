<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 09:02.
 */

namespace App\Controller\Admin;

use App\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends BaseController
{
    public function __invoke(Request $request, Response $response, $args = null)
    {
        return $this->render($response, 'admin/home.html.twig');
    }
}
