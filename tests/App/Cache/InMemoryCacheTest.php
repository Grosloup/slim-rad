<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 21/11/17
 * Time: 22:26
 */

namespace Test\App\Cache;


use App\Cache\InMemoryCache;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class InMemoryCacheTest extends TestCase
{
    /**
     * @var CacheInterface
     */
    private $cache;

    public function setUp()
    {
        $this->cache = new InMemoryCache();
    }
    /**
     * @test
     */
    public function getKey()
    {
        $this->assertNull($this->cache->get('notExists'));

        $this->expectException(InvalidArgumentException::class);
        $this->cache->get(3);

        $this->cache->set('key', 'value');
        $this->assertSame('value', $this->cache->get('key'));

        $this->cache->set('key', 'value', -2);
        $this->assertNull($this->cache->get('key'));

        $this->cache->set('key', 'value', 60);
        $this->assertSame('value', $this->cache->get('key'));
    }

    /**
     * @test
     */
    public function setKey()
    {
        $this->assertTrue($this->cache->set('key', 'value'));
        $this->expectException(InvalidArgumentException::class);
        $this->cache->set(3, 'value');
    }

    /**
     * @test
     */
    public function deleteKey()
    {
        $this->cache->set('key', 'value');
        $this->cache->delete('key');
        $this->assertNull($this->cache->get('key'));
    }

    /**
     * @test
     */
    public function hasKey()
    {
        $this->cache->set('key', 'value');
        $this->assertTrue($this->cache->has('key'));
        $this->assertFalse($this->cache->has('noExists'));
        $this->expectException(InvalidArgumentException::class);
        $this->cache->has(3);
    }

    /**
     * @test
     */
    public function canClearAll()
    {
        $this->cache->set('key', 'value');
        $this->cache->set('key2', 'value');
        $this->cache->set('key3', 'value');
        $this->assertTrue($this->cache->has('key'));
        $this->assertTrue($this->cache->has('key2'));
        $this->assertTrue($this->cache->has('key3'));
        $this->cache->clear();
        $this->assertFalse($this->cache->has('key'));
        $this->assertFalse($this->cache->has('key2'));
        $this->assertFalse($this->cache->has('key3'));
    }

    /**
     * @test
     */
    public function canDeleteMultiple()
    {
        $this->cache->set('key', 'value');
        $this->cache->set('key2', 'value');
        $this->cache->set('key3', 'value');
        $this->assertTrue($this->cache->has('key'));
        $this->assertTrue($this->cache->has('key2'));
        $this->assertTrue($this->cache->has('key3'));
        $this->cache->deleteMultiple(['key', 'key3']);
        $this->assertFalse($this->cache->has('key'));
        $this->assertFalse($this->cache->has('key3'));
        $this->assertTrue($this->cache->has('key2'));
    }

    /**
     * @test
     */
    public function canGetMultiple()
    {
        $this->cache->set('key', 'value');
        $this->cache->set('key2', 'value2');
        $this->cache->set('key3', 'value3');

        $multiple = $this->cache->getMultiple(['key', 'key3']);
        $this->assertCount(2, $multiple);
        $this->assertContains('value', $multiple);
        $this->assertContains('value3', $multiple);
    }
}
