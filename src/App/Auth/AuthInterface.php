<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 09:55.
 */

namespace App\Auth;

interface AuthInterface
{
    public function userExists($username = '');

    public function checkUser();

    public function getUser($username = '', $password = '');

    public function getSessionKey();

    public function unsetUser();

    public function getCurrentUser();
}
