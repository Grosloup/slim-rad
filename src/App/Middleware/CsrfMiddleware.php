<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 16:56.
 */

namespace App\Middleware;

use App\BaseMiddleware;
use App\Session\SessionStorageInterface;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class CsrfMiddleware extends BaseMiddleware
{
    /**
     * @var SessionStorageInterface
     */
    private $sessionStorage;
    /**
     * @var string
     */
    private $sessionKey;
    /**
     * @var string
     */
    private $formKey;
    /**
     * @var callable
     */
    private $csrfFailHandler;
    /**
     * @var int
     */
    private $limit;

    /**
     * CsrfMiddleware constructor.
     *
     * @param ContainerInterface      $container
     * @param SessionStorageInterface $sessionStorage
     * @param string                  $sessionKey
     * @param string                  $formKey
     * @param int                     $limit
     * @param callable|null           $csrfFailHandler
     */
    public function __construct(
        ContainerInterface $container,
        SessionStorageInterface $sessionStorage,
        $sessionKey = 'jetons.crf',
        $formKey = 'jeton_csrf',
        $limit = 50,
        callable $csrfFailHandler = null
    ) {
        parent::__construct($container);
        $this->sessionStorage = $sessionStorage;
        $this->sessionKey = $sessionKey;
        $this->formKey = $formKey;

        $this->createSessionStore();
        if ($csrfFailHandler) {
            $this->csrfFailHandler = $csrfFailHandler;
        } else {
            $this->csrfFailHandler =
                function (Request $request, Response $response) {
                    return $response->write('csrf injection detected !');
                };
        }

        $this->limit = $limit;
    }

    private function createSessionStore()
    {
        if (!$this->sessionStorage->has($this->sessionKey)) {
            $this->sessionStorage->setKey($this->sessionKey, []);
        }
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param $next
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        $params = $request->getParsedBody();
        if (in_array($request->getMethod(), ['POST', 'PUT', 'DELETE', 'PATCH'], true)) {
            if (array_key_exists($this->formKey, $params)) {
                if (in_array($params[$this->formKey], $this->sessionStorage->getKey($this->sessionKey), true)) {
                    $this->generateToken();
                    $this->limitTokenStorage();

                    return $next($request, $response);
                }
                $this->generateToken();
                // Le token n'est pas enregistré
                $response = $response->withStatus(400);

                return ($this->csrfFailHandler)($request, $response);
            }
            $this->generateToken();
            // le token n'existe pas
            $response = $response->withStatus(400);

            return ($this->csrfFailHandler)($request, $response);
        }
        $this->generateToken();
        $this->limitTokenStorage();

        return $next($request, $response);
    }

    /**
     * @throws \Exception
     *
     * @return string
     */
    public function generateToken()
    {
        $token = bin2hex(random_bytes(16));
        $tokens = $this->sessionStorage->getKey($this->sessionKey);
        $tokens[] = $token;
        $this->sessionStorage->setKey($this->sessionKey, $tokens);

        return $token;
    }

    private function limitTokenStorage()
    {
        while (count($this->sessionStorage->getKey($this->sessionKey)) > $this->limit) {
            $tokens = $this->sessionStorage->getKey($this->sessionKey);
            array_shift($tokens);
            $this->sessionStorage->setKey($this->sessionKey, $tokens);
        }
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey;
    }
}
