<?php
/*
 * web routes
 */

use App\Controller\Admin\HomeController;
use App\Controller\Admin\SigninController;
use App\Controller\IndexController;
use App\Middleware\AdminFirewallMiddleware;

$app->get('/', IndexController::class)->setName('home');

$app->group(
    '/back-office',
    function () {
        $this->get('/signin', SigninController::class)->setName('admin.signin');
        $this->post(
            '/signin_check',
            function () {
            }
        )->setName('admin.signin_check');
        $this->get(
            '/signout',
            function () {
            }
        )->setName('admin.signout');
        $this->get('', HomeController::class)->setName('admin.home');
    }
)->add(
    new AdminFirewallMiddleware(
        $app->getContainer(),
        $app->getContainer()->get('secure.admin.auth')
    )
);
