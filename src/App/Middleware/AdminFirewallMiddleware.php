<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/11/17
 * Time: 08:58.
 */

namespace App\Middleware;

use App\Auth\AuthInterface;
use App\BaseMiddleware;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AdminFirewallMiddleware.
 */
class AdminFirewallMiddleware extends BaseMiddleware
{
    /**
     * @var AuthInterface
     */
    private $auth;

    /**
     * AdminFirewallMiddleware constructor.
     *
     * @param ContainerInterface $container
     * @param AuthInterface      $auth
     */
    public function __construct(ContainerInterface $container, AuthInterface $auth)
    {
        parent::__construct($container);
        $this->auth = $auth;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param $next
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     *
     * @return Response|static
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        $path = $request->getUri()->getPath();
        $securityPaths = $this->container->get('security')['admin']['paths'];

        if ($path === $securityPaths['signin_uri']) {
            if ($this->checkUser()) {
                return $response->withRedirect($securityPaths['success_redirect_uri']);
            }
            $response = $next($request, $response);

            return $response;
        }

        if ($path === $securityPaths['signin_check_uri']) {
            if (!$request->isPost()) {
                $this->addMessage('danger', 'Method not allowed');

                return $response->withRedirect($securityPaths['signin_uri']);
            }
            if ($this->checkUser()) {
                return $response->withRedirect($securityPaths['success_redirect_uri']);
            }

            $username = $request->getParam('username', null);
            $password = $request->getParam('password', null);

            if ($this->getUser($username, $password)) {
                return $response->withRedirect($securityPaths['success_redirect_uri']);
            }
            $this->addMessage('danger', 'Unknown user');

            return $response->withRedirect($securityPaths['signin_uri']);
        }

        if ($path === $securityPaths['signout_uri']) {
            $this->signout();
            $this->addMessage('success', 'Vous êtes bien déconnecté.');

            return $response->withRedirect($securityPaths['signin_uri']);
        }

        if ($this->checkUser()) {
            $response = $next($request, $response);

            return $response;
        }
        $this->addMessage('danger', 'Vous devez vous identifier.');

        return $response->withRedirect($securityPaths['signin_uri']);
    }

    /**
     * @return mixed
     */
    private function checkUser()
    {
        return $this->auth->checkUser();
    }

    /**
     * @param $username
     * @param $password
     *
     * @return mixed
     */
    private function getUser($username, $password)
    {
        return $this->auth->getUser($username, $password);
    }

    private function signout()
    {
        $this->auth->unsetUser();
    }
}
