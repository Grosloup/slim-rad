<?php

return [
    'cache_dir' => __DIR__ . '/../../var/cache/twig',
    'templates' => [__DIR__ . '/../templates'],
];
