#SlimRad

create a basic application with Slim micro framework

Slim PHP micro-framework (3.9) with Twig template engine, Twitter Bootstrap (3.3.7), Font Awesome (4.7.0), Gulp

##Install

```
composer create-project --prefer-dist grosloup/slim-rad
```

##Start dev server

```
php -S 127.0.0.1:8888 -t public/ -d display_errors=1
```

Enjoy !
