<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once __DIR__."/../vendor/autoload.php";

$bases = require_once __DIR__ . "/../resources/configs/bases.php";
$env = empty($bases['env']) ? 'development' : $bases['env'];

$params = require_once __DIR__ . "/../resources/configs/database.php";

$config = \Doctrine\ORM\Tools\Setup::createXMLMetadataConfiguration(
    $params['configs_files'],
    $bases['env'] === 'production' ? false : true
);
$dbParams = $params['db_params'][$env];

try {
    $entityManager = \Doctrine\ORM\EntityManager::create(
        $dbParams,
        $config
    );
} catch (\Exception $exception) {
    die($exception->getMessage());
}


return ConsoleRunner::createHelperSet($entityManager);
